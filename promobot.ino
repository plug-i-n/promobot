#define p_ind    13 // standart indication
#define p_pot    A0 // analog potentiometer
#define d_stop   35 // distance to stop in CM
#define d_delta   5 // distance to stop in CM
#define t_speed   1 // time for speed inertia
#define t_mesure 25 // mesurement time
//#define DEBUG   1  // debug mode

#include "Wheelbot.h"
#include "Ultrasonic.h"

Wheelbot bot(4, 5, 6, 7);
Ultrasonic us(11, 12);

float distance = 0;

unsigned long t_cur = 0;
#ifdef DEBUG
unsigned long t_ser = 0;
#endif


void setup(){
	pinMode(p_ind, OUTPUT);
	#ifdef DEBUG
	Serial.begin(9600);
	#endif
	randomSeed(analogRead(5));
}


void ledon()
{
	digitalWrite(p_ind, HIGH);
}

void ledoff()
{
	digitalWrite(p_ind, LOW);
}


void loop(){
	t_cur = millis();
	#ifdef DEBUG
	if(t_cur - t_ser > t_mesure){
		Serial.println(dist_cm);
		t_ser = t_cur;
	}
	#endif
	ledon();
	delay(1000); // startup delay
	ledoff();
	distance = us.Ranging(CM);
	if(distance < d_stop){
		bot.Stop();
		ledon();
		delay(1000);
		ledoff();
		if(random(100)>50){
			while(distance < (d_stop+d_delta)){
				bot.Right(200);
				distance = us.Ranging(CM);
			}
		}else{
			while(distance < (d_stop+d_delta)){
				bot.Left(200);
				distance = us.Ranging(CM);
			}
		}
	}else{
		bot.ForwardRandomly(200);
		delay(25);
	}
}

